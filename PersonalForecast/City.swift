//
//  City.swift
//  PersonalForecast
//
//  Created by Alex Raskopin on 03.07.17.
//  Copyright © 2017 Raskopin. All rights reserved.
//

import UIKit

class City: NSObject {
    
    var name: String
    var country: String
    var code: String
    var degree: Int?
    var weatherIcon: String?
    var weatherDescription: String?
    var latitude: Double?
    var longitude: Double?
    var tempMin: Int?
    var tempMax: Int?
    var windSpeed: Int?
    var sunset: Int?
    var sunrise: Int?
    
    init(name: String, country: String, code: String) {
        self.name = name
        self.country = country
        self.code = code
    }
}
