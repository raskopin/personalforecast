//
//  CityForecastController.swift
//  PersonalForecast
//
//  Created by Admin on 06.07.17.
//  Copyright © 2017 Raskopin. All rights reserved.
//

import UIKit
import MapKit

class CityForecastController: UIViewController {

    var city: City?
    
    @IBOutlet weak var cityCurrentDegree: UILabel!

    @IBOutlet weak var cityCurrentWeatherImage: UIImageView!

    @IBOutlet weak var cityMapView: MKMapView!

    @IBOutlet weak var cityCurrentWeatherDescription: UILabel!
    
    @IBOutlet weak var cityCurrentWindSpeed: UILabel!
    
    @IBOutlet weak var cityTodaySunrise: UILabel!
    
    @IBOutlet weak var cityTodaySunset: UILabel!
    
    @IBOutlet weak var cityTodayTempMAX: UILabel!
    
    @IBOutlet weak var cityTodayTempMIN: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var cityCoordinate: CLLocationCoordinate2D
        var cityMKCoordinateRegion: MKCoordinateRegion
        var cityMKCoordinateSpan: MKCoordinateSpan
        
        if let city: City = self.city {
            let timeFormatter = DateFormatter()
            timeFormatter.dateFormat = "hh:mm"
            if let sunrise = city.sunrise {
                let sunriseTime = Date(timeIntervalSince1970: TimeInterval(sunrise))
                cityTodaySunrise.text = "\(timeFormatter.string(from: sunriseTime))"
            }
            if let sunset = city.sunset {
                let sunsetTime = Date(timeIntervalSince1970: TimeInterval(sunset))
                cityTodaySunset.text = "\(timeFormatter.string(from: sunsetTime))"
            }
            if city.latitude != nil && city.longitude != nil {
                cityCoordinate = CLLocationCoordinate2D(latitude: city.latitude!, longitude: city.longitude!)
                cityMKCoordinateSpan  = MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1)
                cityMKCoordinateRegion = MKCoordinateRegion(center: cityCoordinate, span: cityMKCoordinateSpan)
                DispatchQueue.global().async {
                    DispatchQueue.main.async {
                        self.cityMapView.setRegion(cityMKCoordinateRegion, animated: true)
                    }
                }
            }
            if city.degree != nil {
                cityCurrentDegree.text = ("\(city.degree!)")
            }
            if city.weatherDescription != nil {
                cityCurrentWeatherDescription.text = city.weatherDescription!
            }
            if city.windSpeed != nil {
                cityCurrentWindSpeed.text = "\(city.windSpeed!)"
            }
            if city.tempMin != nil {
                cityTodayTempMIN.text = "\(city.tempMin!)"
            }
            if city.tempMax != nil {
               cityTodayTempMAX.text = "\(city.tempMax!)"
            }
            if city.weatherDescription != nil {
                cityCurrentWeatherImage.image = UIImage(named: city.weatherIcon!)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
