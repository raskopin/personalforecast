//
//  CitiesListTableCell.swift
//  PersonalForecast
//
//  Created by Admin on 05.07.17.
//  Copyright © 2017 Raskopin. All rights reserved.
//

import UIKit

class CitiesListTableCell: UITableViewCell {

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var CityName: UILabel!
    
    @IBOutlet weak var CityCountry: UILabel!
    
    @IBOutlet weak var CityDegree: UILabel!
    
    @IBOutlet weak var CityWeatherIcon: UIImageView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
