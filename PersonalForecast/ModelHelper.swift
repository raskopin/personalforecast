//
//  ModelHelper.swift
//  PersonalForecast
//
//  Created by Alex Raskopin on 08.07.17.
//  Copyright © 2017 Raskopin. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ModelHelper {
    
    static var isFirstAppStart: Bool = true
    static let firebaseDatabaseReference = Database.database().reference()
    
    class func IsCitiesListEmpty(completionHandler: @escaping (_ isCitiesListEmpty: Bool) -> ()) -> Bool {
        var isCitiesListEmpty: Bool = true
        if let userID = FBSDKAccessToken.current().userID {
            firebaseDatabaseReference
                .child("users")
                .child(userID)
                .child("cities")
                .observeSingleEvent(of: .childAdded, with: { (snapshot) in
                    isCitiesListEmpty = false
                    completionHandler(isCitiesListEmpty)
                })
            completionHandler(isCitiesListEmpty)
        }
        return isCitiesListEmpty
    }
    
    class func IsCitiesListOfflineDataEmpty(completionHandler: @escaping (_ isCitiesListEmpty: Bool) -> ()) -> Bool {
        if let offlineData = OfflineHelper.userDataStore {
            if offlineData["users"][FBSDKAccessToken.current().userID]["cities"].count > 0 {
                return false
            }
        }
        return true
    }

}
