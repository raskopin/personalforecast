//
//  OfflineHelper.swift
//  PersonalForecast
//
//  Created by Admin on 07.07.17.
//  Copyright © 2017 Raskopin. All rights reserved.
//

import UIKit
import SwiftyJSON

class OfflineHelper {
    
    static let defaults = UserDefaults.standard
    static var userDataStore : JSON?
    
    class func initOfflineData() {
        if defaults.string(forKey: "OfflineData") == nil || defaults.string(forKey: "OfflineData") == "" {
            defaults.set("", forKey: "OfflineData")
            userDataStore = JSON.parse(defaults.string(forKey: "OfflineData")!)
            userDataStore!.dictionaryObject = [
                "users" : [
                    [:]
                ]
            ]
            saveOfflineData()
        }
        if let stringOfflineData = defaults.string(forKey: "OfflineData") {
            userDataStore = JSON.parse(stringOfflineData)
        }
    }
    
    class func saveOfflineData() {
        if let userDataStore = OfflineHelper.userDataStore {
            let stringUserDataStore = userDataStore.rawString([.castNilToNSNull: true])
            defaults.set(stringUserDataStore, forKey: "OfflineData")
            if let stringOfflineData = defaults.string(forKey: "OfflineData") {
                self.userDataStore = JSON.parse(stringOfflineData)
            }
        }
    }
    
    class func deleteOfflineData() {
        defaults.removeObject(forKey: "OfflineData")
    }
    
}
