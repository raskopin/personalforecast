//
//  CitiesListController.swift
//  PersonalForecast
//
//  Created by Admin on 05.07.17.
//  Copyright © 2017 Raskopin. All rights reserved.
//

import UIKit
import FirebaseDatabase
import SwiftyJSON


class CitiesListController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var numberOfCities: Int = 0
    var citiesList = [City]()
    var firebaseDatabaseReference: DatabaseReference = Database.database().reference()

    @IBOutlet weak var citiesListTableView: UITableView!

    @IBOutlet weak var addCityButton: UIButton!
    

    @IBAction func toLogout(_ sender: AnyObject) {
        performSegue(withIdentifier: "toLogoutSegue", sender: false)
    }

    @IBAction func toAddCity(_ sender: AnyObject) {
        if InternetAvailability.isServicesAvailable {
            performSegue(withIdentifier: "toAddCitySegue", sender: sender)
        }
    }
    
    override func viewDidLoad() {
        fetchCities()
        citiesListTableView.delegate = self
        citiesListTableView.dataSource = self
        citiesListTableView.separatorStyle = .singleLine
        citiesListTableView.tableFooterView = UIView()
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool = true) {
        OfflineHelper.saveOfflineData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.citiesList.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = citiesListTableView.dequeueReusableCell(withIdentifier: "CityNameCell")! as! CitiesListTableCell
        cell.CityCountry.text = self.citiesList[indexPath.row].country
        cell.CityName.text = self.citiesList[indexPath.row].name
        let degree : Int = self.citiesList[indexPath.row].degree! as Int
        cell.CityDegree.text = "\(degree)"
        cell.CityWeatherIcon.image = UIImage(named: self.citiesList[indexPath.row].weatherIcon!)
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor(red:0.58, green:0.65, blue:0.65, alpha:1.0)
        } else {
            cell.backgroundColor = UIColor(red:0.93, green:0.94, blue:0.95, alpha:1.0)
        }
                //if InternetAvailability.isServicesAvailable! {
            //DispatchQueue.global().async {
                //let url = URL(string: "http://openweathermap.org/img/w/\(self.citiesList[indexPath.row].weatherIcon!).png")
                //let data = try? Data(contentsOf: url!)
                //DispatchQueue.main.async {
                    //cell.CityWeatherIcon.image = UIImage(data: data!)
                    //cell.CityWeatherIcon.image = UIImage(cgImage: )
                //}
            //}
        //}

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toSelectCityForecast", sender: citiesList[indexPath.row])
        citiesListTableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if (InternetAvailability.isServicesAvailable) {
                firebaseDatabaseReference
                    .child("users")
                    .child(FBSDKAccessToken.current().userID)
                    .child("cities")
                    .child(citiesList[indexPath.row].code)
                    .removeValue()
                citiesList.remove(at: indexPath.row)
                self.citiesListTableView.reloadData()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if let  controller = segue.destination as? CityForecastController {
                if var selectedCity = sender as? City {
                    if InternetAvailability.isServicesAvailable{
                        let newOW = OpenWeatherHelper(city: selectedCity.name)
                        newOW.fillCityWeatherDetails(city: selectedCity)
                        controller.title = selectedCity.name
                        controller.city = selectedCity
                        if OfflineHelper.userDataStore != nil {
                            if OfflineHelper.userDataStore!["users"][FBSDKAccessToken.current().userID]["cities"][selectedCity.code].exists() {
                                OfflineHelper.userDataStore!["users"][FBSDKAccessToken.current().userID]["cities"][selectedCity.code].dictionaryObject = [
                                    "country": selectedCity.country,
                                    "name": selectedCity.name,
                                    "degree": selectedCity.degree,
                                    "lat": selectedCity.latitude,
                                    "lon": selectedCity.longitude,
                                    "temp_max": selectedCity.tempMax,
                                    "temp_min": selectedCity.tempMin,
                                    "wind": selectedCity.windSpeed,
                                    "sunset": selectedCity.sunset,
                                    "sunrise": selectedCity.sunrise,
                                    "weatherDescription": selectedCity.weatherDescription
                                ]
                            }
                            OfflineHelper.saveOfflineData()
                        }
                    } else {
                        if OfflineHelper.userDataStore != nil {
                            if let city: JSON = OfflineHelper.userDataStore!["users"][FBSDKAccessToken.current().userID]["cities"][selectedCity.code] {
                                if let country: String = city["country"].string {
                                    selectedCity.country = country
                                }
                                if let name: String = city["name"].string {
                                    selectedCity.name = name
                                }
                                if let degree: Int = city["degree"].int {
                                    selectedCity.degree = degree
                                }
                                if let lat: Double = city["lat"].double {
                                    selectedCity.latitude = lat
                                }
                                if let temp_max: Int = city["temp_max"].int {
                                    selectedCity.tempMax = temp_max
                                }
                                if let temp_min: Int = city["temp_min"].int {
                                    selectedCity.tempMin = temp_min
                                }
                                if let wind: Int = city["wind"].int {
                                    selectedCity.windSpeed = wind
                                }
                                if let sunset: Int = city["sunset"].int {
                                    selectedCity.sunset = sunset
                                }
                                if let sunrise: Int = city["sunrise"].int {
                                    selectedCity.sunrise = sunrise
                                }
                                if let weatherDescription: String = city["weatherDescription"].string {
                                    selectedCity.weatherDescription = weatherDescription
                                }
                                controller.title = selectedCity.name
                                controller.city = selectedCity
                            }
                        }
                    }
                }
            }
    }
    
    func fetchCities() {
        if FBSDKAccessToken.current() != nil {
            if InternetAvailability.isServicesAvailable {
                DispatchQueue.global().async {
                    self.firebaseDatabaseReference
                        .child("users")
                        .child(FBSDKAccessToken.current().userID)
                        .child("cities")
                        .observe(.childAdded, with: { (snapshot) in
                            if let city = snapshot.value as? [String : AnyObject] {
                                var cityName: String = ""
                                var cityCountry: String = ""
                                for value in city {
                                    if value.key == "name" {
                                        cityName = value.value as! String
                                    }
                                    if value.key == "country" {
                                        cityCountry = value.value as! String
                                    }
                                }
                                let newCity = City(name: cityName, country: cityCountry, code: snapshot.key)
                                let newOW = OpenWeatherHelper(city: cityName)
                                newOW.fillCityWeather(city: newCity)
                                self.citiesList.append(newCity)
                                if OfflineHelper.userDataStore != nil {
                                        OfflineHelper.userDataStore!["users"][FBSDKAccessToken.current().userID]["cities"][newCity.code].dictionaryObject = [
                                            "country": newCity.country,
                                            "name": newCity.name,
                                            "degree": newCity.degree,
                                            "weatherIcon": newCity.weatherIcon
                                    ]
                                    OfflineHelper.saveOfflineData()
                                }
                                DispatchQueue.main.async {
                                    self.citiesListTableView.reloadData()
                                }
                            }
                        })
                }
            } else {
                if let cities: JSON = OfflineHelper.userDataStore!["users"][FBSDKAccessToken.current().userID]["cities"] {
                    for city in cities {
                        let newCity = City(name: city.1["name"].stringValue, country: city.1["country"].stringValue, code: city.0)
                        newCity.degree = city.1["degree"].intValue
                        newCity.weatherIcon = city.1["weatherIcon"].stringValue
                        self.citiesList.append(newCity)
                    }
                }
            }
            
        }
    }
    
}
