//
//  ViewController.swift
//  PersonalForecast
//
//  Created by Admin on 30.06.17.
//  Copyright © 2017 Raskopin. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseAuth
import FirebaseDatabase
import SwiftyJSON


class ViewController: UIViewController, FBSDKLoginButtonDelegate {

    var currentFacebookProfile: FacebookProfile?
    var firebaseDatabaseReference: DatabaseReference?
    var firebaseDatabaseHandle: DatabaseHandle?
    let loginButton: FBSDKLoginButton = {
        let button = FBSDKLoginButton()
        button.readPermissions=["email"]
        return button
    }()
    
    override func viewDidLoad() {
        //OfflineHelper.deleteOfflineData()
        OfflineHelper.initOfflineData()
        InternetAvailability.isServicesAvailable = InternetAvailability.isConnectedToNetwork()
        super.viewDidLoad()
        firebaseDatabaseReference = Database.database().reference()
        view.addSubview(loginButton)
        loginButton.center = view.center
        loginButton.delegate = self
        self.goAfterSuccessfulLoginToAddCitiesView(self.loginButton)
    }

    func storeCurrentFacebookProfile(completionHandler: @escaping () -> ()){
        if (FBSDKAccessToken.current()) != nil {
            if InternetAvailability.isServicesAvailable {
                var isFacebookProfileExistInFirebase: Bool = false
                self.firebaseDatabaseReference?.child("users").child(FBSDKAccessToken.current().userID).observe(.childAdded, with: { (snapshot) in
                    isFacebookProfileExistInFirebase = true
                })
                
                FacebookHelper.fetchProfile {
                if !isFacebookProfileExistInFirebase {
                    if FacebookHelper.currentFacebookProfile != nil {
                        self.firebaseDatabaseReference?.child("users").child(FacebookHelper.currentFacebookProfile!.id).child("email").setValue(FacebookHelper.currentFacebookProfile!.email)                        
                    }
                    let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                    Auth.auth().signIn(with: credential, completion: nil)
                    }
                    if !OfflineHelper.userDataStore!["users"][FBSDKAccessToken.current().userID].exists() {
                        OfflineHelper.userDataStore!["users"].dictionaryObject =
                        [
                            FBSDKAccessToken.current().userID : [
                                    "email": FacebookHelper.currentFacebookProfile!.email,
                                    "cities": [:]
                            ]
                        ]
                        OfflineHelper.saveOfflineData()
                    }
                    completionHandler()
                }
            } else {
                completionHandler()
            }
        }
    }
    
    @IBAction func goAfterSuccessfulLoginToAddCitiesView(_ sender: UIButton) {
        if ModelHelper.isFirstAppStart {
            self.storeCurrentFacebookProfile() {
                if InternetAvailability.isConnectedToNetwork() {
                    let _ = ModelHelper.IsCitiesListEmpty(){isEmpty -> () in
                        ModelHelper.isFirstAppStart = false
                        if isEmpty {
                            self.performSegue(withIdentifier: "afterLoginSegue", sender: sender)
                        } else {
                            self.performSegue(withIdentifier: "GoToCitiesList", sender: sender)
                        }
                    }
                } else {
                    if !ModelHelper.IsCitiesListOfflineDataEmpty(completionHandler: {_ in }) {
                        ModelHelper.isFirstAppStart = false
                        self.performSegue(withIdentifier: "GoToCitiesList", sender: sender)
                    }
                }
            }
        }
    }
    
    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        self.storeCurrentFacebookProfile() {
            if ModelHelper.isFirstAppStart {
                self.goAfterSuccessfulLoginToAddCitiesView(self.loginButton)
            }
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        FBSDKAccessToken.setCurrent(nil)
    }
    
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
    
    
    
}

