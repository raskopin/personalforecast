//
//  InternetAvailability.swift
//  PersonalForecast
//
//  Created by Admin on 07.07.17.
//  Copyright © 2017 Raskopin. All rights reserved.
//

import Foundation
import SystemConfiguration
import SwiftyJSON


extension URLSession {
    func synchronousDataTask(url: URL) -> (Data?, URLResponse?, Error?) {
        var data: Data?, response: URLResponse?, error: Error?
        
        let semaphore = DispatchSemaphore(value: 0)
        
        let dataTask = self.dataTask(with: url) {
            data = $0
            response = $1
            error = $2
            
            semaphore.signal()
        }
        
        dataTask.resume()
        
        _ = semaphore.wait(timeout: .distantFuture)
        
        return (data, response, error)
    }
}


class InternetAvailability {

    static var userDataStore : JSON?
    
    static var isServicesAvailable: Bool = true

    class func isConnectedToNetwork()->Bool{
        
        var status: Bool = true
        
        let urls = [URL(string: "https://firebase.google.com")!, URL(string: "http://api.openweathermap.org")!, URL(string: "https://facebook.com")!]
        let urlConfig = URLSessionConfiguration.default
        urlConfig.timeoutIntervalForResource = 2
        urlConfig.timeoutIntervalForRequest = 2
        let session = URLSession(configuration: urlConfig)
        for url in urls {
            let res = session.synchronousDataTask(url: url)
            if let httpResponse = res.1 as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    status = false;
                }
            } else {
                status = false;
            }
        }
        return status
    }
}
