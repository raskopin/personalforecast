//
//  FacebookProfile.swift
//  PersonalForecast
//
//  Created by Admin on 04.07.17.
//  Copyright © 2017 Raskopin. All rights reserved.
//

import UIKit

class FacebookProfile {
    var id: String
    var email: String
    init(id: String, email: String) {
        self.id = id
        self.email = email
    }
}
