//
//  OpenWeatherHelper.swift
//  PersonalForecast
//
//  Created by Alex Raskopin on 02.07.17.
//  Copyright © 2017 Raskopin. All rights reserved.
//

import Foundation
import SwiftyJSON

class OpenWeatherHelper {
    var city: String
    var openWeatherData: Data?
    static var citiesOpenWeatherData = [String: Data]()
    init(city: String) {
        self.city = city
    }

    private let openWeatherMapBaseURL = "http://api.openweathermap.org/data/2.5/"
    private let openWeatherMapAPIKey = "a9cb1454467a4773e7ec855c8d8344da"
    let session = URLSession.shared
    private let requestAPITypeWeather = "weather"
    private let requestApiTypeForecast = "forecast"
    
    private func getOpenWeatherRequestURLByCityName(_ typeOfService: String, city: String) -> URL {
        return URL(string: "\(openWeatherMapBaseURL + typeOfService)?APPID=\(openWeatherMapAPIKey)&q=\(city)&units=metric")!
    }
    
    private func getOpenWeatherRequestURLByCityCode(_ typeOfService: String, cityCode: String) -> URL {
        return URL(string: "\(openWeatherMapBaseURL + typeOfService)?APPID=\(openWeatherMapAPIKey)&id=\(cityCode)&units=metric")!
    }
    
    private func ifCityWeatherDataExistInDictionary(code: String) -> Bool {
        if OpenWeatherHelper.citiesOpenWeatherData[code] != nil {
            return true
        }
        return false
    }
    
    private func getOpenWeatherData (openWeatherRequestURL: URL, runTaskSync: @escaping () -> Bool) {
        var res: Bool = false
       
        let dataTask = session.dataTask(with: openWeatherRequestURL) {
            (data: Data?, response: URLResponse?, error: Error?) -> Void in
            if error == nil {
                self.openWeatherData = data
                res = runTaskSync()
            }
        }
        dataTask.resume()
        while !res {
            
        }
    }
    
    func fillCityWeather(city: City) {
        var jsonOpenWeatherSwiftyJSONObject: JSON?
        if ifCityWeatherDataExistInDictionary(code: city.code) {
            let cityOpenWeatherData = OpenWeatherHelper.citiesOpenWeatherData[city.code]
            jsonOpenWeatherSwiftyJSONObject = self.getSwiftyJSONObject(data: cityOpenWeatherData!)
        } else {
            let openWeatherRequestURL = getOpenWeatherRequestURLByCityCode(requestAPITypeWeather, cityCode: city.code)
            getOpenWeatherData(openWeatherRequestURL: openWeatherRequestURL, runTaskSync: {() in
                if self.openWeatherData != nil {
                    if self.getCode(data: self.openWeatherData!) == 200 {
                        jsonOpenWeatherSwiftyJSONObject = self.getSwiftyJSONObject(data: self.openWeatherData!)
                        city.degree = jsonOpenWeatherSwiftyJSONObject!["main"]["temp"].intValue
                        city.weatherIcon = jsonOpenWeatherSwiftyJSONObject!["weather"][0]["icon"].stringValue
                    }
                }
                return true
            })
        }
    }
    
    func fillCityWeatherDetails(city: City) {
        var jsonOpenWeatherSwiftyJSONObject: JSON?
        if ifCityWeatherDataExistInDictionary(code: city.code) {
            let cityOpenWeatherData = OpenWeatherHelper.citiesOpenWeatherData[city.code]
            jsonOpenWeatherSwiftyJSONObject = self.getSwiftyJSONObject(data: cityOpenWeatherData!)
        } else {
            let openWeatherRequestURL = getOpenWeatherRequestURLByCityCode(requestAPITypeWeather, cityCode: city.code)
            getOpenWeatherData(openWeatherRequestURL: openWeatherRequestURL, runTaskSync: {() in
                if self.openWeatherData != nil {
                    if self.getCode(data: self.openWeatherData!) == 200 {
                        jsonOpenWeatherSwiftyJSONObject = self.getSwiftyJSONObject(data: self.openWeatherData!)
                    }
                }
                return true
            })
        }
        city.latitude = jsonOpenWeatherSwiftyJSONObject!["coord"]["lat"].doubleValue
        city.longitude = jsonOpenWeatherSwiftyJSONObject!["coord"]["lon"].doubleValue
        city.tempMax = jsonOpenWeatherSwiftyJSONObject!["main"]["temp_max"].intValue
        city.tempMin = jsonOpenWeatherSwiftyJSONObject!["main"]["temp_min"].intValue
        city.windSpeed = jsonOpenWeatherSwiftyJSONObject!["wind"]["speed"].intValue
        city.sunset = jsonOpenWeatherSwiftyJSONObject!["sys"]["sunset"].intValue
        city.sunrise = jsonOpenWeatherSwiftyJSONObject!["sys"]["sunrise"].intValue
        city.weatherDescription = jsonOpenWeatherSwiftyJSONObject!["weather"][0]["description"].stringValue
    }
    
    func getCity() -> City? {
        var resultCity: City?
        let openWeatherRequestURL = getOpenWeatherRequestURLByCityName(requestAPITypeWeather, city: city)
            getOpenWeatherData(openWeatherRequestURL: openWeatherRequestURL, runTaskSync: {() in
            if self.openWeatherData != nil {
                if self.getCode(data: self.openWeatherData!) == 200 {
                    let jsonOpenWeatherSwiftyJSONObject = self.getSwiftyJSONObject(data: self.openWeatherData!)
                    let jsonOpenWeatherSwiftyJSONCity =  jsonOpenWeatherSwiftyJSONObject["name"].stringValue
                    if self.city.lowercased() == jsonOpenWeatherSwiftyJSONCity.lowercased() {
                        let jsonOpenWeatherSwiftyJSONCode = jsonOpenWeatherSwiftyJSONObject["id"].stringValue
                        let jsonOpenWeatherSwiftyJSONCountry = jsonOpenWeatherSwiftyJSONObject["sys"]["country"].stringValue
                        let city = City(name: jsonOpenWeatherSwiftyJSONCity, country: jsonOpenWeatherSwiftyJSONCountry, code: jsonOpenWeatherSwiftyJSONCode)
                        resultCity = city
                    }
                }
            }
            return true
        })
        return resultCity
    }
    
    func getCode(data: Data) -> Int{
            let jsonOpenWeatherSwiftyJSONObject = getSwiftyJSONObject(data: data)
            let jsonOpenWeatherSwiftyJSONObjectCode = jsonOpenWeatherSwiftyJSONObject["cod"].intValue
            return jsonOpenWeatherSwiftyJSONObjectCode
    }
    
    private func getSwiftyJSONObject(data: Data) -> JSON {
        return JSON(data: data)
    }
    
}
