//
//  AddCitiesController.swift
//  PersonalForecast
//
//  Created by Alex Raskopin on 01.07.17.
//  Copyright © 2017 Raskopin. All rights reserved.
//

import UIKit
import FirebaseDatabase
import SwiftyJSON

class AddCitiesController: UIViewController {
    
    var firebaseDatabaseReference: DatabaseReference?
    var city: City?
    var isCitiesListEmpty: Bool = true
    
    @IBOutlet weak var SearchingResults: UILabel!
    @IBOutlet weak var DoYouWantToAddCityToListLabel: UILabel!
    @IBOutlet weak var AddCityButton: UIButton!
    @IBOutlet weak var FindCityTextField: UITextField!
    @IBOutlet weak var AddCityCancelButton: UIButton!
    @IBOutlet weak var GoToCitiesListButton: UIButton!
    
    override func viewDidLoad() {
        firebaseDatabaseReference = Database.database().reference()
        FindCityTextField.layer.borderWidth = 1.0
        FindCityTextField.layer.cornerRadius = 5
        GoToCitiesListButton.backgroundColor = UIColor(red:0.32, green:0.70, blue:0.85, alpha:0.7)
        self.isCitiesListEmpty = ModelHelper.IsCitiesListEmpty(){_ in }
    }
    
    @IBAction func GoToCitiesListAction(_ sender: UIButton) {
        let isCitiesListEmpty = ModelHelper.IsCitiesListOfflineDataEmpty { (_) in}
        if !isCitiesListEmpty {
            self.performSegue(withIdentifier: "ToCitiesListSegue", sender: sender)
        }
    }
    
    @IBAction func AddCityButtonActon(_ sender: UIButton) {
        if let realResultCity: City = self.city as City! {
            var isCityAlreadyExist: Bool = false
            if let userID = FBSDKAccessToken.current().userID {
                let listener = self.firebaseDatabaseReference!
                    .child("users")
                    .child(userID)
                    .child("cities")
                    .child(realResultCity.code)
                    .observe(.childAdded, with: { (snapshot) in
                        self.changeDialogVisibility(hidden: true)
                        self.SearchingResults.text = ("City \(realResultCity.name) is already in your list")
                        isCityAlreadyExist = true
                        })
                self.firebaseDatabaseReference!
                    .child("users")
                    .child(userID)
                    .child("cities")
                    .child(realResultCity.code)
                    .removeObserver(withHandle: listener)
                if (!isCityAlreadyExist) {
                    self.firebaseDatabaseReference!.child("users")
                        .child(userID)
                        .child("cities")
                        .child(realResultCity.code)
                        .child("country")
                        .setValue(realResultCity.country)
                    self.firebaseDatabaseReference!.child("users")
                        .child(userID)
                        .child("cities")
                        .child(realResultCity.code)
                        .child("name")
                        .setValue(realResultCity.name)
                    self.SearchingResults.text = ("City \(realResultCity.name) was successfuly added!")
                    self.changeDialogVisibility(hidden: true)
                    isCityAlreadyExist = true
                }
                if !OfflineHelper.userDataStore!["users"][FBSDKAccessToken.current().userID]["cities"][realResultCity.code].exists() && isCityAlreadyExist{
                    OfflineHelper.userDataStore!["users"][userID]["cities"][realResultCity.code].dictionaryObject = [
                        "country": realResultCity.country,
                        "name": realResultCity.name
                    ]
                }
                OfflineHelper.saveOfflineData()
            }
        }
    }
    
    @IBAction func AddCityCancelButtonAction(_ sender: UIButton) {
        FindCityTextField.text = nil
        changeDialogVisibility(hidden: true)
        changeFoundCityLabelVisibility(hidden: true)
    }
    
    @IBAction func TestWeather(_ sender: AnyObject) {
        if let city = FindCityTextField.text {
            if city.characters.count > 0 && isValidInput(input: city){
                let newOW = OpenWeatherHelper(city: city)
                let resultCity: City? = newOW.getCity()
                changeFoundCityLabelVisibility(hidden: false)
                if let realResultCity = resultCity as City! {
                    SearchingResults.text = "City is found: \(realResultCity.name) (\(realResultCity.country))"
                    changeDialogVisibility(hidden: false)
                    self.city = realResultCity
                } else {
                    SearchingResults.text = "City is not found"
                }
            }
        }
    }
    
    func changeFoundCityLabelVisibility(hidden: Bool) {
        if hidden {
            if !SearchingResults.isHidden {
                SearchingResults.isHidden = true
            }
        } else {
            if SearchingResults.isHidden {
                SearchingResults.isHidden = false
            }
        }
    }
    
    func changeDialogVisibility(hidden: Bool) {
        if hidden {
            if !DoYouWantToAddCityToListLabel.isHidden{
                DoYouWantToAddCityToListLabel.isHidden = true
            }
            
            if  !AddCityButton.isHidden {
                AddCityButton.isHidden = true
            }
            
            if !AddCityCancelButton.isHidden {
                AddCityCancelButton.isHidden = true
            }
        } else {
            if DoYouWantToAddCityToListLabel.isHidden{
                DoYouWantToAddCityToListLabel.isHidden = false
            }
            
            if  AddCityButton.isHidden {
                AddCityButton.isHidden = false
            }
            
            if AddCityCancelButton.isHidden {
                AddCityCancelButton.isHidden = false
            }
        }
    }
    
    func isValidInput(input: String) -> Bool {
        let regEx = "^\\w{2,20}$"
        let regObject = NSPredicate(format: "SELF MATCHES %@", regEx)
        return regObject.evaluate(with: input)
    }
    
}
