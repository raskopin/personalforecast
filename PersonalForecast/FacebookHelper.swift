//
//  FacebookHelper.swift
//  PersonalForecast
//
//  Created by Admin on 03.07.17.
//  Copyright © 2017 Raskopin. All rights reserved.
//

import UIKit

class FacebookHelper: NSObject {
    static var currentFacebookProfile: FacebookProfile?
    
    class func fetchProfile(completionHandler: @escaping () -> ()){
        
        let parameters = ["fields": "email, first_name, last_name, picture.type(large)"]
        FBSDKGraphRequest(graphPath: "me", parameters: parameters).start { (connection, result, error) -> Void in
            if error != nil {
                completionHandler()
            }
            if let results = result as? [String: AnyObject] {
                if let id: String = results["id"] as? String{
                    if let email: String = results["email"] as? String {
                        let fetchedCurrentFacebookProfile: FacebookProfile = FacebookProfile(id: id, email: email)
                        currentFacebookProfile = fetchedCurrentFacebookProfile
                    }
                }
                completionHandler()
            }
        }
    }
}
